# Exams

The course has two exams one is internal of 15 marks, while the other is final worth 70 marks. Rest marks are part of continuous assessment that belongs to attendance, seminar and assignments. Practice the following question for exams

* Unit 1
    * Calculating contour integral using parametrization of curve.
    * Use path independence when antiderivative exists.
    * Use Cauchy theorem when function is analytic
* Unit 2
    * Use Cauchy integral formula to find integration along closed contour.
    * Use Cauchy integral formula to find integration along closed contour.


## Internal Exam

Internal exam consist of 5 objectives and 3 descriptive questions, out of which you have to answer all objectives and 2 descriptive questions.

## Final Exam

Final exam divided in three groups. The questions are organized in three groups as follows:

| Group |       Questions      |          Marks          | Total |
|-------|:--------------------:|:-----------------------:|-------|
| A     |         10/10        |            2            | 20    |
| B     |         04/05        |            5            | 20    |
| C     |         03/05        |            10           | 30    |
