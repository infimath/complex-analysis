# Welcome to complex analysis course

This page is dedicated to the course of complex analysis taught at the university department of mathematics, Tilka Manjhi Bhagalpur University, Bhagalpur.

## Syllabus

- Contour Integration
- Cauchy Integral Formula and Related Theorems
- Power and Laurents Series
- Singularities
- Rouches theorem

## Objectives

The course ==Complex Analysis== focussed on the study of properties of complex functions. This is the second course on complex analysis in TMBU, Bhagalpur. The first course mainly introduce the concept of differentiability and analyticity in complex domains. Here we will discuss the integration in the sense of complex functions.

- [x] Understand contour integration
- [x] Apply Cauchy integral formula
- [x] Important results based on CIF
- [ ] Ability to represent complex function as Power and Laurents series
- [ ] Understand different type of singularity
- [ ] Rouches theorem and application

## References

- Foundations Of Complex Analysis by S. Ponnusamy
- Complex Variable: Schaum's Outlines Series
- [Complex Analysis by Beck](http://math.sfsu.edu/beck/papers/complex.pdf)

## Routine

| Weekday | 1st Period | 2nd Period | 3rd Period | 4th Period | 5th Period |
|---------|------------|------------|------------|------------|------------|
| MON     |            |            | CA         |            |            |
| TUE     |            |            | CA         |            |            |
| WED     | CA         |            |            |            |            |
| THR     | CA         |            |            |            |            |
| FRI     |            | CA         |            |            |            |
| SAT     |            | CA         |            |            |            |

## Instructor

[Sandeep Suman](https://sandeepsuman.com), Assistant Professor, TMBU, Bhagalpur
